package com.example.aashu.dynamiclistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText ed1;
    ListView mylist;
    Button b1;


    ArrayList<String> arrayList=new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        

        ed1=findViewById(R.id.edittext1);
        mylist=findViewById(R.id.mylist);
        b1=findViewById(R.id.button1);

        adapter=new ArrayAdapter<String>(this ,android.R.layout.simple_list_item_1,arrayList);

        mylist.setAdapter(adapter);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {


                String name=ed1.getText().toString();
                arrayList.add(name);
                adapter.notifyDataSetChanged();



            }
        });

    }
}
